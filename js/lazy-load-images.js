const targets = document.querySelectorAll('img');

const lazyLoad = (target) => {
  const io = new IntersectionObserver((entries, observer) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        const img = entry.target;
        const dateLazy = img.getAttribute('data-src');

        if (dateLazy) {
          img.setAttribute('src', dateLazy);
        }

        observer.disconnect();
      }
    });
  });

  io.observe(target);
};

targets.forEach(lazyLoad);
