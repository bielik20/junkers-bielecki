﻿import 'ol/ol.css';
import View from 'ol/View';
import Feature from 'ol/Feature';
import Map from 'ol/Map';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import OSM from 'ol/source/OSM';
import { fromLonLat } from 'ol/proj';
import Point from 'ol/geom/Point';
import Icon from 'ol/style/Icon';
import Style from 'ol/style/Style';
import { defaults } from 'ol/interaction';
import marker from '../images/marker-red.png';

const center = fromLonLat([17.013682, 52.35514]);

new Map({
  target: 'map',
  interactions: defaults({ mouseWheelZoom: false, dragPan: false }),
  layers: [
    new TileLayer({
      source: new OSM(),
    }),
    new VectorLayer({
      source: new VectorSource({
        features: [new Feature({ geometry: new Point(center) })],
      }),
      style: new Style({
        image: new Icon({
          anchor: [0.7, 36],
          anchorXUnits: 'fraction',
          anchorYUnits: 'pixels',
          src: marker,
        }),
      }),
    }),
  ],
  view: new View({
    center,
    zoom: 17,
  }),
});
