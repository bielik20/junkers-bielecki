﻿//Hides navbar on click
$('.nav a').on('click', function () {
  $('.navbar-collapse').collapse('hide');
});

document.querySelectorAll('.remove-focus').forEach((element) =>
  element.addEventListener('mouseup', (e) => e.target.blur()),
);
