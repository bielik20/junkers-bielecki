﻿const navbarLinks = document.querySelectorAll(".navbar a, footer a[href='#home']");

navbarLinks.forEach((link) => {
  const hash = link.hash;

  link.addEventListener('click', (event) => {
    // Prevent default anchor click behavior
    event.preventDefault();

    window.scrollTo({
      top: document.querySelector(hash).offsetTop,
      behavior: 'smooth',
    });
  });
});
