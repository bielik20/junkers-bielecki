﻿const slideTarget = (target) => {
  const io = new IntersectionObserver(
    (entries, observer) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          entry.target.classList.add('slide');
          observer.disconnect();
        }
      });
    },
    { threshold: 0.1 },
  );

  io.observe(target);
};

const elements = document.querySelectorAll('.slideanim');

elements.forEach(slideTarget);
