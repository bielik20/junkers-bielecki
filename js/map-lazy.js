const io = new IntersectionObserver((entries, observer) => {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
      import('./map').then(() => null);
      observer.disconnect();
    }
  });
})

io.observe(document.querySelector('#map'));
