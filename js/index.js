import 'bootstrap/js/carousel'
import 'bootstrap/js/collapse'
import 'bootstrap/js/transition'
import './scrolling';
import './site';
import './slide';
import './map-lazy';
import './lazy-load-images';
